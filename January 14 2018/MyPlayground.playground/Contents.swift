//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/* Create a shuffle function */

func shuffle( array: [Any] ) -> [Any] // O( N ^ 2 )
{
    var mutableArray : [Any] = Array(array)
    var shuffledArray : [Any] = []
    
    while mutableArray.count > 0 // O(N)
    {
        let index = Int(arc4random()) % mutableArray.count
        
        shuffledArray.append(mutableArray[index])
        
        mutableArray.remove(at: index) // O(N)
    }
    
    return shuffledArray
}

print(shuffle(array: [1,2,3,4,5,6] ))

func fasterShuffle( array: [Any] ) -> [Any] // O(N)
{
    var mutableArray : [Any] = Array(array)
    
    var count = mutableArray.count - 1
    
    while count > 0 // O(N)
    {
        let index = Int(arc4random()) % count
        
        mutableArray.swapAt(index, count)
        
        count = count - 1
    }
    
    return mutableArray
}

print(fasterShuffle(array: [1,2,3,4,5,6] ))
print(fasterShuffle(array: [1,2,3,4,5,6] ))
