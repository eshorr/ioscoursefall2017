//
//  ViewController.swift
//  NavigationControllerExample
//
//  Created by Eric Shorr on 12/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
        print("number of view controllers \(self.navigationController?.viewControllers.count)")
    }

    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backToRoot(_ sender: Any)
    {
        self.navigationController?.popToRootViewController(animated: true)
    }
    

}

