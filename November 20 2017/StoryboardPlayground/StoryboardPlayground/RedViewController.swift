//
//  RedViewController.swift
//  StoryboardPlayground
//
//  Created by Eric Shorr on 20/11/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class RedViewController: UIViewController {

    @IBOutlet weak var lblYouFoundMe: UILabel!
    
    var youFoundMe = "You found meghjghj."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblYouFoundMe.text = youFoundMe
    }
    
    @IBAction func back(_ sender: Any)
    {
      self.navigationController?.popViewController(animated: true)
    }
    

}
