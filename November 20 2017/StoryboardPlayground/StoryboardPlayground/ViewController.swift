//
//  ViewController.swift
//  StoryboardPlayground
//
//  Created by Eric Shorr on 20/11/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var txtField: UITextField!
    
    @IBAction func nextView(_ sender: Any)
    {
        self.performSegue(withIdentifier: "redSegue", sender: nil)
    }
    
    @IBAction func goBlue(_ sender: Any)
    {
        self.performSegue(withIdentifier: "blueSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "redSegue"
        {
            if let vc : RedViewController = segue.destination as? RedViewController
            {
                if let text : String = self.txtField.text
                {
                    vc.youFoundMe = text
                }

            }
        }
        
    }
}

