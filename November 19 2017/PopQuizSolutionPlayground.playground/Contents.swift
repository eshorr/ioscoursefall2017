//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/* How do you print the string above? */

print(str)

/* Name some basic data types. */

// integer, double, float, string, character, bool

/* What is the difference between an array and a dictionary? */

// array is range of values and dictionary has keys to values

/* Create a class for a Person with name, age, height, gender, and race as its properties */

enum Gender {
    case male, female
}

enum Race {
    case white, black, hispanic, asian
}

class Person
{
    var name : String?
    var height: Double?
    var gender: Gender?
    var race: Race?
}

/* Describe and/or code binary search. */

func BinarySearch (value: Int, array: [Int]) -> Bool
{
    let midPoint : Int = array.count / 2
    
    if( value == array[midPoint] )
    {
        print("You found me")
        return true
    }
    else if( array.count == 1 )
    {
        print("I’m not here")
        return false
    }
    else if( value < array[midPoint] )
    {
        var leftArray : [Int] = []
        
        for i in 0...(midPoint-1)
        {
            leftArray.append(array[i])
        }
        
        return BinarySearch(value: value, array: leftArray)
    }
    else if( value > array[midPoint] )
    {
        var rightArray : [Int] = []
        
        for i in (midPoint+1) ... (array.count - 1)
        {
            rightArray.append(array[i])
        }
        
        return BinarySearch(value: value, array: rightArray)
    }
    
    return false
}


/* Imagine you have two array a = [1,2,3,4,5] and b =[3,2,9,3,7], write a function to find out common elements in both array. */

func findCommonElements( array1: [Int], array2: [Int] ) -> [Int]
{
    var dict : Dictionary<Int,Bool> = Dictionary()
    
    var newArray:[Int] = []
    
    for i in array1
    {
        if dict[i] == nil
        {
            dict[i] = true
        }
    }
    
    for i in array2
    {
        if dict[i] == true
        {
            newArray.append(i)
            dict[i] = false
        }
    }
    
    return newArray
}

var a = [1,2,3,4,5]
var b = [3,2,9,3,7]

var c = findCommonElements(array1: a, array2: b)

print(c)

