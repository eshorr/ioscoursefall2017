//: Playground - noun: a place where people can play

import UIKit

/* Create a stack class */
class Stack
{
    var array: [Any] = []
    
    func push( item: Any )
    {
        self.array.append(item)
    }
    
    func pop ( ) -> Any
    {
        var lastitem = self.array.last
        self.array.removeLast()
        return lastitem
    }
    
    func isEmpty () -> Bool
    {
        return self.array.count == 0
    }
}


/* Create a queue class */
class Queue
{
    var array : [Any] = []
    func enqueue( item : Any)
    {
        self.array.append(item)
    }
    func dequeue() -> Any
    {
        var firstelement = self.array.first
        self.array.removeFirst()
        return firstelement
        
    }
    
    func isEmpty () -> Bool
    {
        return self.array.count == 0
    }
}

/* Linked list class */
class LinkedList
{
    var value : Any?
    var link : LinkedList?
    
    func find( item: Any ) -> Bool
    {
        var linkList: LinkedList = self
        
        repeat
        {
            if( item == linkList.value )
            {
                return true
            }
            
            linkList = linkList.link
            
        } while linkList != nil
        
        return false
    }
    
    func last( ) -> LinkedList?
    {
        return nil
    }
    
    func add( item: Any )
    {
        
    }
    
    
}
