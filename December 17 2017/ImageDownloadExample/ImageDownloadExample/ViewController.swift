//
//  ViewController.swift
//  ImageDownloadExample
//
//  Created by Eric Shorr on 17/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var imageView: UIImageView!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let url : URL = URL(string: "http://www.petsworld.in/blog/wp-content/uploads/2015/03/How-To-Make-Your-Puppy-Gain-Weight.jpg" )!
        
        var urlRequest : URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        self.indicator.startAnimating()
        
//        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let data : Data = data
            {
                sleep(UInt32(10.0)) // pauses the thread for 10 seconds so that we can see the animation
                
                DispatchQueue.main.async { // ensures task is performed on main thread!
                    
                    self.indicator.stopAnimating()
                    
                    if let image : UIImage = UIImage(data: data)
                    {
                        self.imageView.image = image
                    }
                    
                }
                
            }
        
        }
        task.resume() // this makes the task actually run

        
    }

    


}

