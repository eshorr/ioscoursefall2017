//
//  ViewController.swift
//  AnimationDemos
//
//  Created by Eric Shorr on 13/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: BaseViewController
{
    @IBOutlet weak var redView: UIView!
    
    @IBAction func hideView(_ sender: Any)
    {
        UIView.animate(withDuration: 2.0) {
            self.redView.alpha = 0.0
        }
    }
    
    @IBAction func showView(_ sender: Any)
    {
        UIView.animate(withDuration: 2.0) {
            self.redView.alpha = 1.0
        }
    }
    
    @IBAction func moveRight(_ sender: Any)
    {
       
        var frame : CGRect = self.redView.frame
        
        let xDifference = self.view.frame.size.width - self.redView.frame.size.width
        
        frame.origin.x = xDifference
        
        UIView.animate(withDuration: 2.0) {
            self.redView.frame = frame
        }
    }
    
    @IBAction func moveLeft(_ sender: Any)
    {
        var frame : CGRect = self.redView.frame
        
        frame.origin.x = 0
        UIView.animate(withDuration: 2.0) {
            self.redView.frame = frame
        }
    }
    
    @IBAction func moveDown(_ sender: Any)
    {
        var frame : CGRect = self.redView.frame
        
        let yDifference = self.view.frame.size.height - self.redView.frame.size.height
        
        frame.origin.y = yDifference
        
        UIView.animate(withDuration: 2.0) {
            self.redView.frame = frame
        }
    }
    
    @IBAction func moveUp(_ sender: Any)
    {
        var frame : CGRect = self.redView.frame
        
        frame.origin.y = 0
        UIView.animate(withDuration: 2.0) {
            self.redView.frame = frame
        }
    }
    
    @IBAction func moveCorner(_ sender: Any) {
        
        var frame : CGRect = self.redView.frame
        
        let xDifference = self.view.frame.size.width - self.redView.frame.size.width
        
        let yDifference = self.view.frame.size.height - self.redView.frame.size.height
        
        frame.origin.y = yDifference
        
        frame.origin.x = xDifference
        
        UIView.animate(withDuration: 2.0) {
            self.redView.frame = frame
        }
    }
    
    @IBAction func makeIntoSquare(_ sender: Any)
    {
        UIView.animate(withDuration: 2.0) {
            self.redView.layer.cornerRadius = 0
        }
    }
    
    @IBAction func makeIntoCircle(_ sender: Any)
    {
        UIView.animate(withDuration: 2.0) {
            self.redView.layer.cornerRadius = self.redView.frame.size.height / 2
        }
    }
}

