//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

extension String
{
    func plurize() -> String
    {
        struct Holder {
            static var irregularWords : Dictionary<String,String> = [
                "sheep": "sheep",
                "ox" : "oxen",
                "person" : "people",
                "fish" : "fishes",
                "child" : "children"
            ]
        }
        
        let index = self.index(self.startIndex, offsetBy: 0)
        
        
        let firstCharacter = self[index]
        print(firstCharacter)
        
        if let irregularWord : String = Holder.irregularWords[self.lowercased()]
        {
            let secondIndex = irregularWord.index(irregularWord.startIndex, offsetBy: 1)
            let lastIndex = irregularWord.index(irregularWord.lastIndex, offsetBy: 0)
            
            let subString = irregularWord[secondIndex..lastIndex]
            
            return "\(firstCharacter)\(subString)"
            
//            return irregularWord
//            var newIrregularWord : String = irregularWord
//            newIrregularWord[index] = firstCharacter
//
//
//            return newIrregularWord
        }
        
        return "\(self)s"
    }
}

let apple : String = "apple"
let sheep : String = "sheep"
let ox : String = "Ox"

print(apple.plurize())
print(sheep.plurize())
print(ox.plurize())



