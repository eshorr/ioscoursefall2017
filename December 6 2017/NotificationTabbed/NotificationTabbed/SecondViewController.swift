//
//  SecondViewController.swift
//  NotificationTabbed
//
//  Created by Eric Shorr on 06/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

enum OutletError : Error {
    case isNil
}

class SecondViewController: UIViewController
{
    @IBOutlet weak var lblMessage: UILabel!
    
    var message:String?
    
    // this will load what needs to be loaded before viewdidload (especially needed in tab controller)
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        NotificationCenter.default.addObserver(self, selector: #selector(messageReceived(_:)), name: NSNotification.Name(rawValue: "MessageToViewController2"), object: nil)
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if( self.message != nil )
        {
            self.lblMessage.text = self.message
            self.message = nil
        }
    }

    @objc func messageReceived(_ notificiation: NSNotification)
    {
        let txtMessage : String = notificiation.object as! String
        
        print(txtMessage)
        
//        if( self.lblMessage != nil )
//        {
//            self.lblMessage.text = txtMessage
//        }
//        else
//        {
//            self.message = txtMessage
//        }
        
        do
        {
            try self.checkOutlet(outlet: self.lblMessage)
            self.lblMessage.text = txtMessage
        }
        catch OutletError.isNil
        {
            self.message = txtMessage
        }
        catch _ as Error
        {
            // nothing here
        }
    }
    
    func checkOutlet( outlet : UIView? ) throws
    {
        if outlet == nil
        {
            throw OutletError.isNil
        }
        else
        {
            print("All good")
        }
    }
}

