//
//  FirstViewController.swift
//  NotificationTabbed
//
//  Created by Eric Shorr on 06/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var txtFldMessage: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func sendMessage(_ sender: Any)
    {
        self.txtFldMessage.resignFirstResponder()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageToViewController2"), object: self.txtFldMessage.text)
    }
    


}

