//
//  BlueViewController.swift
//  SlideMenuExample
//
//  Created by Eric Shorr on 28/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class BlueViewController: BaseViewController {

    @IBAction func menu(_ sender: Any)
    {
        self.viewDeckController?.open(.left, animated: true)
    }
    

}
