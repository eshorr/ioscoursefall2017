//
//  SlideMenuExampleViewController.swift
//  SlideMenuExample
//
//  Created by Eric Shorr on 27/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit
import ViewDeck

class SlideMenuExampleViewController: IIViewDeckController
{
    
    
    lazy var menuController : UIViewController = {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: "MenuController")
        
    }()
    
    lazy var redController : UIViewController = {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: "RedController")
        
    }()
    
    lazy var blueController : UIViewController = {
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        return storyboard.instantiateViewController(withIdentifier: "BlueController")
        
    }()

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        
        self.leftViewController = self.menuController
        self.centerViewController = self.redController
        
        self.isPanningEnabled = false
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    

}
