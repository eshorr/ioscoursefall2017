//
//  MenuViewController.swift
//  SlideMenuExample
//
//  Created by Eric Shorr on 28/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class MenuViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource
{
    let menuItems = MenuItem.loadSampleFromPlist()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }
    
    @IBAction func red(_ sender: Any?)
    {
        if let slideMenuController : SlideMenuExampleViewController = self.viewDeckController as? SlideMenuExampleViewController
        {
            slideMenuController.centerViewController = slideMenuController.redController
            slideMenuController.closeSide(true)
        }
    }
    
    @IBAction func blue(_ sender: Any?)
    {
        if let slideMenuController : SlideMenuExampleViewController = self.viewDeckController as? SlideMenuExampleViewController
        {
            slideMenuController.centerViewController = slideMenuController.blueController
            slideMenuController.closeSide(true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let menuItem = self.menuItems[indexPath.row]
        
        cell.textLabel?.text = menuItem.title
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let menuItem = self.menuItems[indexPath.row]
        
        self.perform(menuItem.selector, with: nil)
        
    }
    
}
