//
//  ViewController.swift
//  SlideMenuExample
//
//  Created by Eric Shorr on 27/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: BaseViewController
{
    
    @IBAction func menu(_ sender: Any)
    {
        self.viewDeckController?.open(.left, animated: true)
    }
    
    @IBAction func tapMe(_ sender: Any)
    {
        print("That tickles")
    }
    
    @IBAction func handlePan(recognizer:UIPanGestureRecognizer)
    {
        let translation = recognizer.translation(in: self.view)
        if let view = recognizer.view {
            view.center = CGPoint(x:view.center.x + translation.x,
                                  y:view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @IBAction func longPressMe(_ sender: Any)
    {
        print("That hurts")
    }
    
    @IBAction func handlePinch(_ recognizer: UIPinchGestureRecognizer)
    {
        if let view = recognizer.view
        {
            print("pinch")
            
            view.transform = view.transform.scaledBy(x: recognizer.scale, y: recognizer.scale)
            recognizer.scale = 1
        }
    }
    
    @IBAction func handleRotation(_ recognizer: UIRotationGestureRecognizer)
    {
        if let view = recognizer.view
        {
            print("rotate")
            
            
            view.transform = view.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    
}

