//
//  Category+CoreDataProperties.swift
//  GiftCategoryExample
//
//  Created by Eric Shorr on 26/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var name: String?
    @NSManaged public var gifts: NSSet?

}

// MARK: Generated accessors for gifts
extension Category {

    @objc(addGiftsObject:)
    @NSManaged public func addToGifts(_ value: Gift)

    @objc(removeGiftsObject:)
    @NSManaged public func removeFromGifts(_ value: Gift)

    @objc(addGifts:)
    @NSManaged public func addToGifts(_ values: NSSet)

    @objc(removeGifts:)
    @NSManaged public func removeFromGifts(_ values: NSSet)

}
