//
//  Gift+CoreDataProperties.swift
//  GiftCategoryExample
//
//  Created by Eric Shorr on 26/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//
//

import Foundation
import CoreData


extension Gift {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Gift> {
        return NSFetchRequest<Gift>(entityName: "Gift")
    }

    @NSManaged public var name: String?
    @NSManaged public var price: Double
    @NSManaged public var categories: NSSet?

}

// MARK: Generated accessors for categories
extension Gift {

    @objc(addCategoriesObject:)
    @NSManaged public func addToCategories(_ value: Category)

    @objc(removeCategoriesObject:)
    @NSManaged public func removeFromCategories(_ value: Category)

    @objc(addCategories:)
    @NSManaged public func addToCategories(_ values: NSSet)

    @objc(removeCategories:)
    @NSManaged public func removeFromCategories(_ values: NSSet)

}
