//
//  AddCategoryViewController.swift
//  GiftCategoryExample
//
//  Created by Eric Shorr on 26/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class AddCategoryViewController: BaseViewController {

    @IBOutlet weak var txtField: UITextField!
    
    @IBAction func cancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addCategory(_ sender: Any)
    {
        let category = Category(context: self.managedContext)
        category.name = self.txtField.text
        
        self.appDelegate.saveContext()
        
        self.dismiss(animated: true, completion: nil)
    }
    

}
