//
//  GiftsViewController.swift
//  GiftCategoryExample
//
//  Created by Eric Shorr on 27/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class GiftsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,AddGiftDelegate {

    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var tableView : UITableView!
    
    var category : Category?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.tableView.register(NSClassFromString("UITableViewCell"), forCellReuseIdentifier: "Cell")
        
        self.navBar.topItem?.title = self.category?.name
    }
    
    @IBAction func back(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addGift(_ sender: Any)
    {
        self.performSegue(withIdentifier: "addGiftSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (self.category?.gifts?.count)!
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let obj : Gift = self.category?.gifts?.allObjects[indexPath.row] as! Gift
        
        cell.textLabel?.text = obj.name
        
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc : AddGiftsViewController = segue.destination as? AddGiftsViewController
        {
            vc.delegate = self
        }
    }
    
    func addNewGift(gift: Gift)
    {
        self.category?.addToGifts(gift)
        self.appDelegate.saveContext()
        
        self.tableView.reloadData()
    }
}
