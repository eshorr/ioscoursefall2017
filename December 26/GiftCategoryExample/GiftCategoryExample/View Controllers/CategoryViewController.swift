//
//  CategoryViewController.swift
//  GiftCategoryExample
//
//  Created by Eric Shorr on 26/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit
import CoreData

class CategoryViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate
{

    @IBOutlet weak var tableView : UITableView!
    
//    let appDelegate : AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
//    let context : NSManagedObjectContext = appDelegate.persistentContainer.viewContext
    
    var fetchedResultsController: NSFetchedResultsController<Category>?
    
    
    
    @IBAction func addCategory(_ sender: Any?)
    {
        self.performSegue(withIdentifier: "addCategorySegue", sender: nil)
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let request: NSFetchRequest<Category> = Category.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(
            key: "name",
            ascending: true,
            selector: #selector(NSString.localizedCaseInsensitiveCompare(_:))
            )]
        
        self.fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: self.managedContext, sectionNameKeyPath: nil, cacheName: "CategoryCache")
        
        self.fetchedResultsController?.delegate = self
        
        self.tableView.register(NSClassFromString("UITableViewCell"), forCellReuseIdentifier: "Cell")
        
        self.loadData()
    }
    
    func loadData()
    {
        try? fetchedResultsController?.performFetch() // would be better to catch errors!
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj : Category = self.fetchedResultsController!.object(at: indexPath) as Category
        
        self.performSegue(withIdentifier: "giftsSegue", sender: obj)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if let sections = fetchedResultsController?.sections, sections.count > 0
        {
            return sections[section].numberOfObjects
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let obj : Category = self.fetchedResultsController!.object(at: indexPath) as Category
        
        cell.textLabel?.text = obj.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            let obj : Category = self.fetchedResultsController!.object(at: indexPath) as Category
            
            self.managedContext.delete(obj)
            
            self.appDelegate.saveContext()
            
//            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    // Supposed to reload the data whenever anything gets added or changed
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
    {
        self.tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc : GiftsViewController = segue.destination as? GiftsViewController
        {
            vc.category = sender as? Category
        }
    }
}
