//
//  AddGiftsViewController.swift
//  GiftCategoryExample
//
//  Created by Eric Shorr on 27/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

protocol AddGiftDelegate : NSObjectProtocol
{
    func addNewGift(gift: Gift)
}

class AddGiftsViewController: BaseViewController {

    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtName: UITextField!
    
    weak var delegate : AddGiftDelegate?
    
    @IBAction func cancel(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addGift(_ sender: Any)
    {
        guard let price : Double = Double(self.txtPrice.text!)
            else {
                return
        }
        
        let gift = Gift(context: self.managedContext)
        gift.name = self.txtName.text
        gift.price = price
        
        self.delegate?.addNewGift(gift: gift)
        
        self.dismiss(animated: true, completion: nil)
    }
}
