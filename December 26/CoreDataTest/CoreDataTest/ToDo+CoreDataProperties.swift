//
//  ToDo+CoreDataProperties.swift
//  CoreDataTest
//
//  Created by Eric Shorr on 26/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//
//

import Foundation
import CoreData


extension ToDo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ToDo> {
        return NSFetchRequest<ToDo>(entityName: "ToDo")
    }

    @NSManaged public var title: String?
    @NSManaged public var summary: String?
    @NSManaged public var dueDate: NSDate?

}
