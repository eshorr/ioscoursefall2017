//
//  ViewController.swift
//  AutolayoutAnimations
//
//  Created by Eric Shorr on 14/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    @IBOutlet weak var blueView: UIView!
    
    @IBOutlet weak var blueViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var blueViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var blueViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var blueViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var blueViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var blueViewTrailingConstraint: NSLayoutConstraint!
    
    @IBAction func moveUp(_ sender: Any)
    {
        
        self.blueViewTopConstraint.constant = 0
        
        UIView.animate(withDuration: 2.0) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    @IBAction func moveDown(_ sender: Any)
    {
        self.blueViewTopConstraint.constant = self.blueView.frame.size.height - self.view.frame.size.height
        
        UIView.animate(withDuration: 2.0) {
            self.view.layoutIfNeeded()

        }
    }
    
    
    
    @IBAction func moveToRight(_ sender: Any)
    {
        // Old school way
//        var frame : CGRect = self.blueView.frame
//
//        let xDifference = self.view.frame.size.width - self.blueView.frame.size.width
//
//        frame.origin.x = xDifference
//
//        UIView.animate(withDuration: 2.0) {
//            self.blueView.frame = frame
//        }
        
        
        // DOES NOT WORK!!!
//        UIView.animate(withDuration: 2.0) {
//            self.blueViewLeadingConstraint.constant = self.view.frame.size.width - self.blueView.frame.size.width
//        }
        
        self.blueViewLeadingConstraint.constant = self.blueViewTrailingConstraint.constant;
        self.blueViewTrailingConstraint.constant = 0
        
//        self.blueViewLeadingConstraint.constant = self.view.frame.size.width - self.blueView.frame.size.width
        
        UIView.animate(withDuration: 2.0) {
            self.view.layoutIfNeeded() // resets the view rendering; creating the display for the user to look at
        }
        
        
    
        
    }
    
    @IBAction func moveLeft(_ sender: Any) {
        
        self.blueViewTrailingConstraint.constant = self.blueViewLeadingConstraint.constant
        self.blueViewLeadingConstraint.constant = 0
        
        UIView.animate(withDuration: 2.0) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @IBAction func expandWidth(_ sender: Any) {
//        self.blueViewWidthConstraint.constant = self.view.frame.size.width
        
        self.blueViewTrailingConstraint.constant = 0
        self.blueViewLeadingConstraint.constant = 0
        
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
        
        }
    
    }
    
    @IBAction func retractWidth(_ sender: Any) {
        
        self.blueViewWidthConstraint.constant = 128.0
        
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            
        }
    }
    @IBAction func expandHeight(_ sender: Any) {
        self.blueViewHeightConstraint.constant = self.view.frame.size.height
        
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            
        }
    }
    
    
    
    @IBAction func retractHeight(_ sender: Any) {
        
        self.blueViewHeightConstraint.constant = 128
        
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            
        }
        
    }
    
}
