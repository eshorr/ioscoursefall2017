//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// please email the quiz to shorr.eric@gmail.com

/* What is the difference between bounds and frame? */

/* Bounds are the internal coordinates of a uiview and the frame is the external coordinates in relation to its superview */


/* What can a subclass do that an extension can not do? */

/* A subclass can add additional properties. An extension can override and extend functions through swizzling */

/*:
 Create a closure assigned to a constant `greeting` that accepts a `name` string argument with no return value. Within the body of the closure, print the argument. Call the closure four times using "Gary", "Jane", "Rick", and "Beth" as arguments.
 */

let greeting = {(name: String) -> Void in
    print("Hello, \(name).")
}

greeting("Gary")
greeting("Jane")
greeting("Rick")
greeting("Beth")

/*:
 Using the code below, use the `filter` function to create a new array of `String` values. The new array should only include Strings longer than four characters. Print the resulting collection.
 */
let schoolSubjects = ["Math", "Computer Science", "Gym", "English", "Biology"]

let filteredSchoolSubjects = schoolSubjects.filter { (name) -> Bool in
    return name.count < 4
}

print(filteredSchoolSubjects)

/* What does the alpha define in a UIView? */

/* Alpha defines the transperency of the uiview. 0 being fully transperent while 1 being fully non-transperent. */

/* How do you go back to the previous screen? */

/* If on a UINavigationController: popToViewController, popToRootController, popViewController.
 
 If modal: dismiss
 
 rewindSegue, using default navbar back button
 
 */

/* What's the difference between push/show segue and a present modal segue? */

/* push/show -- pushes a view controller onto the navigation controller stack but presents modally if there is no navigation controller present.
 
   present modal -- shows the viewcontroller modally.
 
 */

/* Create a function in an extension of an Array to do a binary search */

extension Array
{
    func BinarySearch(searchKey:Int) -> Bool
    {
        let midPoint : Int = self[self.count / 2] as! Int
        
        if midPoint == searchKey
        {
            return true
        }
        else if( self.count == 1 )
        {
            return false
        }
        else if( searchKey < midPoint )
        {
            var lowerArray : Array = []
            
            for i in 0...self.count/2-1
            {
                lowerArray.append(self[i])
            }
            
            return lowerArray.BinarySearch(searchKey: searchKey)
        }
        else if( searchKey > midPoint )
        {
            var upperArray : Array = []
            
            for i in self.count/2+1...self.count-1
            {
                upperArray.append(self[i])
            }
            
            return upperArray.BinarySearch(searchKey: searchKey)
        }
        
        return false
    }
}

let searchArray : Array = [5,7,9,10,22,55,67,99]

print(searchArray.BinarySearch(searchKey: 7))
print(searchArray.BinarySearch(searchKey: 3))
