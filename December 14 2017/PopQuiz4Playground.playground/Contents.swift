//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// please email the quiz to shorr.eric@gmail.com

/* What is the difference between bounds and frame? */


/* What can a subclass do that an extension can not do? */

/*:
 Create a closure assigned to a constant `greeting` that accepts a `name` string argument with no return value. Within the body of the closure, print the argument. Call the closure four times using "Gary", "Jane", "Rick", and "Beth" as arguments.
 */

/*:
 Using the code below, use the `filter` function to create a new array of `String` values. The new array should only include Strings longer than four characters. Print the resulting collection.
 */
let schoolSubjects = ["Math", "Computer Science", "Gym", "English", "Biology"]


/* What does the alpha define in a UIView? */


/* How do you go back to the previous screen? */


/* What's the difference between push/show segue and a present modal segue? */


/* Create a function in an extension of an Array to do a binary search */

