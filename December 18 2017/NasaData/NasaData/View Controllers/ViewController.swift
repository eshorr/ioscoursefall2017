//
//  ViewController.swift
//  NasaData
//
//  Created by Eric Shorr on 18/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage

class ViewController: BaseViewController {

    @IBOutlet weak var txtViewExplanation: UITextView!
    @IBOutlet weak var imgPlanet: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let url : URL = URL(string: "https://api.nasa.gov/planetary/apod?date=2005-2-22&api_key=DEMO_KEY" )!
        
        var urlRequest : URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        MBProgressHUD.showAdded(to: self.mainView, animated: true)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let data : Data = data
            {
                sleep(UInt32(10.0)) // pauses the thread for 10 seconds so that we can see the animation
                
                
                
                DispatchQueue.main.async { // ensures task is performed on main thread!
                    
                    if let rawJSON = try? JSONSerialization.jsonObject(with: data)
                    {
                        print(rawJSON)
                        
                        if let json : [String : Any] = rawJSON as? [String : Any]
                        {
                            if let planet : Planet = Planet(json: json)
                            {
                                self.lblTitle.text = planet.title
                                self.txtViewExplanation.text = planet.explanation
                                
                                self.imgPlanet.sd_setImage(with: planet.hdurl, completed: nil)
//                                self.loadImage(url: planet.hdurl)
                            }
                        }
                        
                    }
                    
                    MBProgressHUD.hide(for: self.mainView, animated: true)
                }
                
            }
            
        }
        task.resume() // this makes the task actually run
    }
    
    func loadImage(url: URL)
    {
        var urlRequest : URLRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let data : Data = data
            {
                DispatchQueue.main.async { // ensures task is performed on main thread!
                    
                    if let image : UIImage = UIImage(data: data)
                    {
                        self.imgPlanet.image = image
                    }
                    
                }
                
            }
            
        }
        task.resume() // this makes the task actually run
    }
    
}



