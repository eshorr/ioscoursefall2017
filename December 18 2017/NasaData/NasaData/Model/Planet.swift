//
//  Planet.swift
//  NasaData
//
//  Created by Eric Shorr on 18/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

/*
 {
 "date": "2005-02-22",
 "explanation": "Are Saturn's auroras like Earth's?  To help answer this question, the Hubble Space Telescope and the Cassini spacecraft monitored Saturn's South Pole simultaneously as Cassini closed in on the gas giant in January 2004.  Hubble snapped images in ultraviolet light, while Cassini recorded radio emissions and monitored the solar wind.  Like on Earth, Saturn's auroras make total or partial rings around magnetic poles.  Unlike on Earth, however, Saturn's auroras persist for days, as opposed to only minutes on Earth.  Although surely created by charged particles entering the atmosphere, Saturn's auroras also appear to be more closely modulated by the solar wind than either Earth's or Jupiter's auroras.  The above sequence shows three Hubble images of Saturn each taken two days apart.",
 "hdurl": "https://apod.nasa.gov/apod/image/0502/saturnauroras_hst_big.jpg",
 "media_type": "image",
 "service_version": "v1",
 "title": "Persistent Saturnian Auroras",
 "url": "https://apod.nasa.gov/apod/image/0502/saturnauroras_hst.jpg"
 }
 */

class Planet: NSObject
{
    var date:Date
    var explanation:String
    var hdurl:URL
    var media_type:String
    var service_version:String
    var url:URL
    var title:String
    
    init(date:Date, explanation:String, hdurl:URL, media_type:String,service_version:String,url:URL,title:String)
    {
        self.date = date
        self.explanation = explanation
        self.hdurl = hdurl
        self.media_type = media_type
        self.service_version = service_version
        self.url = url
        self.title = title
    }
    
    convenience init?(json : [String : Any] )
    {
        guard let dateString = json["date"] as? String else {
            return nil
        }
        
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-DD"
        
        guard let date : Date = dateFormatter.date(from: dateString) else {
            return nil
        }
        
        guard let explanation = json["explanation"] as? String else {
            return nil
        }
        
        guard let hdurlString = json["hdurl"] as? String else {
            return nil
        }
        
        guard let hdurl : URL = URL(string: hdurlString) else {
            return nil
        }
        
        guard let urlString = json["url"] as? String else {
            return nil
        }
        
        guard let url : URL = URL(string: urlString) else {
            return nil
        }
        
        guard let title = json["title"] as? String else {
            return nil
        }
        
        guard let media_type = json["media_type"] as? String else {
            return nil
        }
        
        guard let service_version = json["service_version"] as? String else {
            return nil
        }
        
        self.init(date: date, explanation: explanation, hdurl: hdurl, media_type: media_type, service_version: service_version, url: url, title: title)
    }
}
