//
//  GooglePlacesTableViewController.swift
//  GetTaxi
//
//  Created by Eric Shorr on 18/01/2018.
//  Copyright © 2018 Developer Institute. All rights reserved.
//

import UIKit
import GooglePlaces

class GooglePlacesTableViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate
{
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView:UITableView!
    
    var delayTimer : Timer = Timer()
    var searchResults : [GMSAutocompletePrediction] = []
    
    let placesClient : GMSPlacesClient = GMSPlacesClient()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.searchResults.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let searchResult : GMSAutocompletePrediction = self.searchResults[indexPath.row]
        
        cell.textLabel?.attributedText = searchResult.attributedFullText
        
        return cell
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        self.delayTimer.invalidate()
        
        guard let text : String = searchBar.text,
            text.count > 2
        else
        {
            return
        }
        
        self.delayTimer = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false, block: { (timer) in
            
            self.searchText(text)
        })
    }
    
    func searchText( _ searchText : String )
    {
        self.placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (predictions, error) in
            
            
            if let predictions : [GMSAutocompletePrediction] = predictions
            {
                self.searchResults = predictions
            }
            else if let err : Error = error
            {
                print(err)
            }
            else
            {
                self.searchResults = []
            }
            
            self.tableView.reloadData()
            
        }
    }
    

}
