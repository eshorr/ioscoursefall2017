//: Playground - noun: a place where people can play

import UIKit

// Please email the quiz to shorr.eric@gmail.com

/* What is your name? */
/* Eric Shorr */

/* What is difference between synchronous and asynchronous loading? */
/*
 Synchronous loading goes in a specific step-by-step order while asynchronous loading is allowed to have multiple loadings at once.
 */

/* What is a race condition? */

//when a process need to occure after a thread completed without any check that the thread has completed

/* What is dead lock? */
/*
 A lock occurs when multiple processes try to access the same resource at the same time.
 
 One process loses out and must wait for the other to finish.
 
 A deadlock occurs when the waiting process is still holding on to another resource that the first needs before it can finish.
 
 So, an example:
 
 Resource A and resource B are used by process X and process Y
 
 X starts to use A.
 X and Y try to start using B
 Y 'wins' and gets B first
 now Y needs to use A
 A is locked by X, which is waiting for Y
 The best way to avoid deadlocks is to avoid having processes cross over in this way. Reduce the need to lock anything as much as you can.
 
 In databases avoid making lots of changes to different tables in a single transaction, avoid triggers and switch to optimistic/dirty/nolock reads as much as possible.
 */

/* Can a uiview or one of its subclasses like a uilabel be changed on a background thread? */

/* No. All changes done at the UI level must be done on the main thread */

/* How do you ensure that code is ran on the main thread? */

DispatchQueue.main.async {
    // your code on main thread here.
}

/* What is Cocoapods and why are they good to use? */

/* It is a third party system for retrieving 3rd party libraries and it's good for keeping those libraries up to date */

/* How do you animate the movement of a UIView if it has autolayout contraints? */

// update constraint constants here then call:

UIView.animate(withDuration: 1.0) {
    self.view.layoutIfNeeded()
    
}

/* What is the difference between try, try!, and try? ? */

/* try: Takes responsibility of handling errors
 try! : if there is an error in my code, with "try!", my app can be crash
 try? : It returns an optional and catches error by returning nil
 */

/* What is the difference between an optional and non-optional variable? */

/* An optional can be nil while a non-optional can't be nil. */

/* Propose the model for your upcoming project */

/* A rough example for the DI project: */

class Person : NSObject
{
    var name : String?
    var courses : [Course]?
}

class Teacher : Person
{
    var students : [Student]?
}

class Student : Person
{
    
}

class Course : NSObject
{
    
}

