//: Playground - noun: a place where people can play

import UIKit

// Please email the quiz to shorr.eric@gmail.com

/* What is your name? */

/* What is difference between synchronous and asynchronous loading? */


/* What is a race condition? */


/* What is dead lock? */

/* Can a uiview or one of its subclasses like a uilabel be changed on a background thread? */


/* How do you ensure that code is ran on the main thread? */

/* What is Cocoapods and why are they good to use? */


/* How do you animate the movement of a UIView if it has autolayout contraints? */


/* What is the difference between try, try!, and try? ? */

/* What is the difference between an optional and non-optional variable? */


/* Propose the model for your upcoming project */

