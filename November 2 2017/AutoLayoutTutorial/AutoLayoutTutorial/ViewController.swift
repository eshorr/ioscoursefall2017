//
//  ViewController.swift
//  AutoLayoutTutorial
//
//  Created by Eric Shorr on 02/11/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.btn1.layer.cornerRadius = self.btn1.frame.size.height / 4; // creates rounded corners
        self.btn2.layer.cornerRadius = self.btn2.frame.size.height / 4;
    }

    @IBAction func buttonPressed(_ sender: Any)
    {
        print("that tickles!!!")
    }
    
}

