//: Playground - noun: a place where people can play

import UIKit

/* Declare an array of integers 2,3,5,78,342,32,4 */

var intArray = [2,3,5,78,342,32,4]

/* Create a sorting function using bubble sort */

func bubbleSort(_ arrayToSort : [Int] ) -> [Int] {
    var sorted = arrayToSort
    var toSort = arrayToSort.count
    
    repeat {
        var previousSwap = 0
        for i in 1..<toSort {
            if sorted[i-1] > sorted[i]{
                sorted.swapAt(i, i-1)
                print(sorted)
                previousSwap = i
            }
        }
        toSort = previousSwap
    } while toSort != 0
    
    return sorted
}

var sortedArray = bubbleSort(intArray)

print(sortedArray)

/* Sort the array that was created using the sort function then print the array */

/* Create a search function using binary sort */

/* Use the search function to find 3, 5, 32, and 43 then print the results of their findings */

/* Bonus: Create a sorting function using insertion sort */
