//: Playground - noun: a place where people can play

import UIKit

/* Write a function that separates a given string into space-separated string using a given dictionary.
 
 Given: "thequickbrownfoxjumpsoverthelazydog."
 Given: Dictionary with all of the words in the English language
 Output: "the quick brown fox jumps over the lazy dog."
 */

// stephane's code
func separateWord(array : String, dictionary : Dictionary<String, String>) -> String
{
    var temp = ""
    var receivedString = array
    var newString = ""
    
    while(receivedString.count != 0)
    {
        temp.append(receivedString.first!)
        // receivedString =  receivedString.dropFirst()
        receivedString.remove(at: receivedString.startIndex)
        
        print(temp)
        
        // print(dictionary [temp])
        
        if(dictionary [temp] != nil){
            newString.append("\(temp) ")
            temp.removeAll()
        }
    }
    
    
    return newString
    
}

var array = "thequickbrownfoxjumpsoverthelazydog."
var dic = ["quick":"quick","the":"the","fox":"fox","jumps":"jumps","lazy":"lazy","dog":"dog","brown":"brown","over":"over",".":"."]

let newString = separateWord(array : array, dictionary : dic)
print(newString)


// adrien's code

var dictionnary = ["the","quick","brown","fox","jumps","the","over","lazy","dog."]

func sentenceSort(_ sentence: String) {
    var newSentence: String = ""
    
    for i in 0..<dictionnary.count {
        
        if sentence.contains(dictionnary[i])
        {
            
            newSentence.append(dictionnary[i])
            newSentence.append(" ")
            
        }
        
    }
    print("\(newSentence)")
    
}

sentenceSort(array)
