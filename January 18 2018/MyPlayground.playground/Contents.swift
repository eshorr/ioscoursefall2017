//: Playground - noun: a place where people can play

import UIKit

/* Suppose, that you have an array of 100 numbers such as [1, 2, 3, 4, … 99, 100]. Now suppose that you were to randomly remove one number and shuffle the array. How would you find that missing number? */

func fasterShuffle( array: [Any] ) -> [Any] // O(N)
{
    var mutableArray : [Any] = Array(array)
    
    var count = mutableArray.count - 1
    
    while count > 0 // O(N)
    {
        let index = Int(arc4random()) % count
        
        mutableArray.swapAt(index, count)
        
        count = count - 1
    }
    
    return mutableArray
}

var numbers100 : [Int] = []

for i in 1...100
{
    numbers100.append(i)
}

let ranIndex : Int = Int(arc4random()) % numbers100.count

print("missing number will be \(numbers100[ranIndex])")

numbers100.remove(at: ranIndex)

numbers100 = fasterShuffle(array: numbers100) as! [Int]

var missingNumber = 0

// brute force

for i in 1...100 // O(N)
{
    var isFound : Bool = false
    
    for j in numbers100 // O(N) --> 0(N^2)
    {
        if j == i
        {
            isFound = true
            break
        }
    }
    
    if isFound == false
    {
        missingNumber = i
        print("the missing number is \(missingNumber)")
        break
    }
}

// elegant solution for O(N)

let sum100 = 100 * 101 / 2 // sum of all number from 1 .. 100

missingNumber = sum100

for i in numbers100
{
    missingNumber = missingNumber - i
}

print(missingNumber)


