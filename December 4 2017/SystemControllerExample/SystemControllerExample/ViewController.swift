//
//  ViewController.swift
//  SystemControllerExample
//
//  Created by Eric Shorr on 04/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var imageView:UIImageView!

    @IBOutlet weak var btnShare: UIButton!
    @IBAction func takePhoto(_ sender: Any)
    {
        let picker : UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        
        picker.sourceType = .camera
        
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func getPhotoFromLibrary(_ sender: Any)
    {
        let picker : UIImagePickerController = UIImagePickerController()
        picker.delegate = self
        
        picker.sourceType = .photoLibrary
        
        self.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
            else { return }
        self.imageView.image = selectedImage
        
        self.btnShare.isEnabled = true
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func email(_ sender: Any)
    {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        let mailVC : MFMailComposeViewController = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        
        if self.btnShare.isEnabled // checking to see that the image isn't nil
        {
            let imageData: Data = UIImagePNGRepresentation(self.imageView.image!) as! Data
            
            mailVC.addAttachmentData(imageData, mimeType: "png", fileName: "mySharedImage")
        }
        
        self.present(mailVC, animated: true, completion: nil)
    }
    
    @IBAction func shareImage(_ sender: UIView)
    {
        guard let image = self.imageView.image else { return }
        let activityController =
            UIActivityViewController(activityItems: [image],
                                     applicationActivities: nil)
        activityController.popoverPresentationController?.sourceView
            = sender
        
        present(activityController, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?)
    {
        controller.dismiss(animated: true, completion: nil)
    }
}

