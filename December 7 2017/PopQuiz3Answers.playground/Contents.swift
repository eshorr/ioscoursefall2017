//: Playground - noun: a place where people can play

import UIKit

/* How can two objects communicate with each other? */

/* Delegates, nsnotifications, KVO (key-value-observer) */

/*:
 Create a protocol called `Vehicle` with two requirements: a nonsettable `numberOfWheels` property of type `Int`, and a function called `drive()`.
 */

protocol Vehicle {
    var numberOfWheels: Int {get}
    func drive()
}

/*:
 Define a `Car` struct that implements the `Vehicle` protocol. `numberOfWheels` should return a value of 4, and `drive()` should print "Vroom, vroom!" Create an instance of `Car`, print its number of wheels, then call `drive()`.
 */

class Car: NSObject, Vehicle, NSCoding
{
    var wheels:Int
    var engine:String
    var doors:Int
    var color:UIColor
    var speed:Float
    
    init(wheels:Int, engine:String, doors:Int, color:UIColor, speed:Float)
    {
        self.wheels = wheels
        self.engine = engine
        self.doors = doors
        self.color = color
        self.speed = speed
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(wheels, forKey: "wheels")
        aCoder.encode(engine, forKey: "engine")
        aCoder.encode(doors, forKey: "doors")
        aCoder.encode(color, forKey: "color")
        aCoder.encode(speed, forKey: "speed")
    }
    
    required convenience init?(coder aDecoder: NSCoder)
    {
        guard let wheels = aDecoder.decodeObject(forKey: "wheels") as? Int,
            let engine = aDecoder.decodeObject(forKey: "engine") as? String,
            let doors = aDecoder.decodeObject(forKey: "doors") as? Int,
            let color = aDecoder.decodeObject(forKey: "color") as? UIColor,
        let speed = aDecoder.decodeObject(forKey: "speed") as? Float
            else {
                return nil
        }
        
        self.init(wheels: wheels, engine: engine, doors: doors, color: color, speed: speed)
    }
    
    var numberOfWheels: Int {
        return 4
    }
    
    func drive() {
        print("Vroom, vroom!")
    }
}

let car = Car(wheels: 4, engine: "v8", doors: 4, color: UIColor.red, speed: 300)
car.drive()

/* Describe the MVC design pattern and how it's used */

/* MVC == Model View Controller.
 The model contains all of the data;
 The view contains all of the user interfaces and what is shown to the user.
 The controller mediates between the model and the view.
 
 It is used primarily for code organization of most iOS applications plus web and android.
 */

/* Describe the Singleton and how it's used */

/*
 It is a single instance of a class that can be accessed throughout the application. It's used for sharing data between different objects as well like a global variable.
 */

/* Name a few ways to save data */

/* UserDefaults, NSCoder, SQLLite Database, Core Data, files, plist */

/* Implement the NSCoding protocol on Car */

/* What class does a tableview inherit from or what is a tableview a subclass of? */

/* UIScrollView */

/* What are some advantages and disadvantages of using a xib file over a storyboard? */

/* 1. It is easier for working in groups by splitting viewcontrollers into individual xib files.
   2. It always easier UI managment and not having everything in one big messy and possibly unmanageable file.
   3. It is possilbe to have multiple lose fitting views.
 
 Disadvantages: Can't have segues
 
 */

/* What are the advantages and disadvantages of notifications over delegates? */

/* Notifications are easier to write and program, but are more difficult to maintain in that it may hard to realize which objects are listening and not, which can cause lots of _interesting_ results */
