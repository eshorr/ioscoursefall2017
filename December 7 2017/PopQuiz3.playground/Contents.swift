//: Playground - noun: a place where people can play

import UIKit

/* How can two objects communicate with each other? */


/*:
 Create a protocol called `Vehicle` with two requirements: a nonsettable `numberOfWheels` property of type `Int`, and a function called `drive()`.
 */


/*:
 Define a `Car` struct that implements the `Vehicle` protocol. `numberOfWheels` should return a value of 4, and `drive()` should print "Vroom, vroom!" Create an instance of `Car`, print its number of wheels, then call `drive()`.
 */

/* Describe the MVC design pattern and how it's used */

/* Describe the Singleton and how it's used */

/* Name a few ways to save data */

/* Implement the NSCoding protocol on Car */

/* What class does a tableview inherit from? */

/* What are some advantages and disadvantages of using a xib file over a storyboard? */

/* What are the advantages and disadvantages of notifications over delegates? */
