//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


func foo()
{
    struct StaticVars {
        static var counter = 0
    }
    print(StaticVars.counter)
    StaticVars.counter = StaticVars.counter + 1
}

foo()
foo()
foo()
