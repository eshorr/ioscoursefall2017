//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Athlete : CustomStringConvertible {
    var description: String {
        return "\(name) is \(age) years old and plays for the \(team) in the \(league)."
        
    }
    
    var name : String
    var age : Int
    var league : String
    var team : String
    
    init(name: String, age: Int, league: String, team: String) {
        self.name = name
        self.age = age
        self.league = league
        self.team = team
    }
}

class AthleteManager {
    private var athletes : [Athlete] = []
    
    var name = "Roger Goodell"
    
    static let sharedInstance = AthleteManager()
    
    // static causes the singleton/variable to be created ONCE and ONLY ONCE!
    // let will keep the variable constant
    
    private init()
    {
        print("I have been created")
    }
    
    public func getAthletes() -> [Athlete]
    {
        return self.athletes
    }
    
    public func fireAthlete(athlete: Athlete)
    {
        // remove athlete
    }
    
    public func hireAthlete(athlete: Athlete )
    {
        self.athletes.append(athlete)
    }
}

print(AthleteManager.sharedInstance.name)

AthleteManager.sharedInstance.name = "Joe Shmow"

print(AthleteManager.sharedInstance.name)

AthleteManager.sharedInstance.hireAthlete(athlete: Athlete(name: "Michael Jordan", age: 54, league: "NBA", team: "Chicago Bulls"))

print(AthleteManager.sharedInstance.getAthletes())

