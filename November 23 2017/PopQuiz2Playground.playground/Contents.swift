//: Playground - noun: a place where people can play

import UIKit

// shorr.eric@gmail.com

/* Describe and/or name some sorting algorithms. */

// Bubble sort, insertion sort
// Both are O(N^2)

// Bubble sort compares each element side by side iteratively until there no more swaps being made
// insertion sort compares and puts the elements into new sort array. (see pdfs for details)

// Merge sort, quick sort
// Both are O( N log N )

/* O(1) -- constant speed, best case
   O(N) -- as fast as there as many nodes
   O(N^2) -- usually the slowest
 for( int i = 0; i < array.count; i++ )
 {
    for( int j = 0; j < array.count; j++ )
    {
        // do something
    }
 }
   O( log N ) -- faster than O(N)
   O( N log N ) -- faster than O( N ^ 2)
 
 */


   let array = [1,2,3,4,5]

    var index:Int = 0

    print(array.count)

    for _ in 0..<array.count
    {
        for _ in 0..<array.count
        {
            index = index + 1
        }
    }

    print(index)

/* What is a queue? */

// First in, first out data structure.

// enqueue puts an item into it
// dequeue returns an item and removes from the queue

/* What is a stack? */

// last in, first out data structure

// push puts an item into it
// pop returns an item at the top and removes from the stack


/* What is a linked list? */

/* a dynamic data structure that allows one node to be linked to another
 node until it's tail is nil.
 
 a -> b -> c -> d -> e ... */

/* Is a navigation controller a queue, stack, or linked list? */

// stack

// navation conroller stores its viewcontrollers in a stack. the top of the stack is the visible view controller and to go another viewcontroller on the navigation controller, a push function must be called or a pop must be called.
// e.g. functions pushViewConroller(viewcontroller: animated:), popViewConroller, popToRoot, etc

/* What is the difference between push/show segue and a present modal segue? */

// push/show puts a viewcontroller onto the navigation stack or goes modally if there is no navigation controller .
// present modal segue presents a viewcontroller modally over the previous viewcontroller usually in a bottom to top animation.

/* Which function will run first, viewDidLoad() or viewDidAppear() ? */

// viewDidLoad() -- see Ch. 3.8

/* Can a tab bar controller contain a navigation controller? */

// yes

/* Can a navigation controller contain a tab bar controller? */

// yes
