//: Playground - noun: a place where people can play

import UIKit

/* Write a function to detect if an integer is a palindrome, if the integer were to be reversed, it would still be the same. e.g. 121 is a palindrome, but 456 isn’t.
 
 WITHOUT converting it to a string.
 
 */

func isPalindrome( num: Int ) -> Bool
{
    // reverse the number then check and see if the reversed number is equal to the original value
    
    
    var reversedNumber : Int = 0
    var numberToCheck : Int = num
    
    while numberToCheck > 0
    {
        let remainder = numberToCheck % 10
        reversedNumber = reversedNumber * 10 + remainder
        
        numberToCheck = numberToCheck / 10
    }
    
    return reversedNumber == num
}

isPalindrome(num: 121)
isPalindrome(num: 456)
isPalindrome(num: -1)
