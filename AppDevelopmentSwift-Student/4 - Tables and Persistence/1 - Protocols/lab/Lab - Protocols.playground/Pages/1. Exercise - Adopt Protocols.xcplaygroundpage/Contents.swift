/*:
 ## Exercise - Adopt Protocols: CustomStringConvertible, Equatable, and Comparable
 
 Create a `Human` class with two properties: `name` of type `String`, and `age` of type `Int`. You'll need to create a memberwise initializer for the class. Initialize two `Human` instances.
 */
class Human: CustomStringConvertible, Equatable, Comparable {
    static func <(lhs: Human, rhs: Human) -> Bool {
        return lhs.age < rhs.age
    }
    
    
    static func ==(lhs: Human, rhs: Human) -> Bool {
        return lhs.description == rhs.description
    }
    
    
    
    var age: Int
    var name: String
    
    init(age: Int, name: String){
        self.name = name
        self.age = age
    }
    
    var description: String{
        return "Hey ! it's \(name) he's \(age) old"
    }
}

let firstHuman = Human(age: 40, name: "Edouard")
let secondHuman = Human(age: 34, name: "Eyal")

/*:
 Make the `Human` class adopt the `CustomStringConvertible`. Print both of your previously initialized `Human` objects.
 */
print(firstHuman)
print(secondHuman)

/*:
 Make the `Human` class adopt the `Equatable` protocol. Two instances of `Human` should be considered equal if their names and ages are identical to one another. Print the result of a boolean expression evaluating whether or not your two previously initialized `Human` objects are equal to eachother (using `==`). Then print the result of a boolean expression evaluating whether or not your two previously initialized `Human` objects are not equal to eachother (using `!=`).
 */
if firstHuman == secondHuman
{
    print("all humans are equal")
}
else
{
    print("god created men and women, but colt made them equal")
}

/*:
 Make the `Human` class adopt the `Comparable` protocol. Sorting should be based on age. Create another three instances of a `Human`, then create an array called `people` of type `[Human]` with all of the `Human` objects that you have initialized. Create a new array called `sortedPeople` of type `[Human]` that is the `people` array sorted by age.
 */
let thirdHuman = Human(age: 20, name: "John")
let fourthHuman = Human(age: 41, name: "Leo")
let fifthHuman = Human(age: 32, name: "Sarah")

var people = [firstHuman, secondHuman, thirdHuman, fourthHuman, fifthHuman]
var sortedPeople: [Human] = people.sorted(by: <)

print(people)
print(sortedPeople)
//: page 1 of 5  |  [Next: App Exercise - Printable Workouts](@next)
