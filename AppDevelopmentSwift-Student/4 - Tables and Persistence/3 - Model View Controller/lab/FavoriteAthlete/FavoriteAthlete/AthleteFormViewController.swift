//
//  AthleteFormViewController.swift
//  FavoriteAthlete
//
//  Created by Eric Shorr on 27/11/2017.
//

import UIKit

class AthleteFormViewController: UIViewController {

    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var leagueTextField: UITextField!
    @IBOutlet weak var teamTextField: UITextField!
    
    
    var athlete: Athlete?
    
    func updateView(){
        guard let athlete = athlete else {return}
        nameTextField.text = athlete.name
        ageTextField.text = String(athlete.age)
        leagueTextField.text = athlete.league
        teamTextField.text = athlete.team
        
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func saveButton(_ sender: Any) {
        guard let name = nameTextField.text else {return}
        guard let age: Int = Int(ageTextField.text!) else {return}
        guard let league = leagueTextField.text else {return}
        guard let team = teamTextField.text else {return}
        athlete = Athlete(name: name, age: age, league: league, team: team)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
