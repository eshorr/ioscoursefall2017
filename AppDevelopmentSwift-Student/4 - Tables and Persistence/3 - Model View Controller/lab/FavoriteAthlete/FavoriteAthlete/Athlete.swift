//
//  Athlete.swift
//  FavoriteAthlete
//
//  Created by Eric Shorr on 27/11/2017.
//

import Foundation

class Athlete : CustomStringConvertible {
    var description: String {
        return "\(name) is \(age) years old and plays for the \(team) in the \(league)."
        
    }
    
    var name : String
    var age : Int
    var league : String
    var team : String
    
    init(name: String, age: Int, league: String, team: String) {
        self.name = name
        self.age = age
        self.league = league
        self.team = team
    }
}
