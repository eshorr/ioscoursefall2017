/*:
 ## Exercise - Type Casting and Inspection
 
 Create a collection of type [Any], including a few doubles, integers, strings, and booleans within the collection. Print the contents of the collection.
 */
var items : [Any] = [2.32, 4.5, 6, 18, "cool", "good", true, false]
print(items)

/*:
 Loop through the collection. For each integer, print "The integer has a value of ", followed by the integer value. Repeat the steps for doubles, strings and booleans.
 */
for item in items {
    if let nbr = item as? Int {
        print("The integer has a value of \(nbr)")
    }
    if let double  = item as? Double {
        print("the double has a value of \(double)")
    }
    if let string = item as? String {
        print("the string are \(string)")
    }
    if let bool = item as? Bool {
        print("the bool are \(bool)")
    }
}

/*:
 Create a [String : Any] dictionary, where the values are a mixture of doubles, integers, strings, and booleans. Print the key/value pairs within the collection
 */
var dictio : [String:Any] = ["moyenne":19, "you are intelligent":true,"double":23.5, "pi": 3.14]
print(dictio)

/*:
 Create a variable `total` of type `Double` set to 0. Then loop through the dictionary, and add the value of each integer and double to your variable's value. For each string value, add 1 to the total. For each boolean, add 2 to the total if the boolean is `true`, or subtract 3 if it's `false`. Print the value of `total`.
 */

var total : Double = 0
for item in  dictio.values {
    if item is String{
        total += 1
    }
    else if let boolItem = item as? Bool
    {
        if( boolItem )
        {
            total += 2
        }
        else
        {
            total -= 3
        }
    }
    else if let intItem = item as? Int
    {
        total += Double(intItem)
    }
    else if let doubleItem = item as? Double
    {
        total += doubleItem
    }
}

print(total)

/*:
 Create a variable `total2` of type `Double` set to 0. Loop through the collection again, adding up all the integers and doubles. For each string that you come across during the loop, attempt to convert the string into a number, and add that value to the total. Ignore booleans. Print the total.
 */
var total2: Double
total2 = 0

for i in dictio.values {
    if let int = i as? Int {
        total2 += Double(int)
    }
    else if let double = i as? Double {
        total2 += double
    }
    else if let stringNbr = i as? String {
        if let numberStr = Double(stringNbr)
        {
            total2 += numberStr
        }
    }
}
print(total2)

//: page 1 of 2  |  [Next: App Exercise - Workout Types](@next)
