//: Playground - noun: a place where people can play

import UIKit

/*
 Using the % operator, create function to determine if a number is even or odd . (Hint 5 % 4 == 1 ; 7 % 5 == 2 ; 6 % 3 == 0 )
 */


/*
 Write a function that would count to N and print the number, but instead print "fizz" whenever it is a multiple of 3 and "buzz" when it is a multiple of 5. e.g. 1, 2, fizz, 3, 4, buzz, fizz, 7, 8, fizz, buzz
 */


/* Using a loop, create a function that will create a pyramid of *'s like this:
 
     *
    * *
   * * *
  * * * *
 
 */

/* Write a function that would calculate factorials.
    0! = 1
    1! = 1
    2! = 1 x 2
    3! = 1 x 2 x 3
    10! = 1 x 2 x 3 x ... x 9 x 10
 */


/* Challenge: Rewrite this function using recursion. (A function that calls itself)
 */

/* Challenge: Write a function to get an array of all the prime numbers less than 100 */




