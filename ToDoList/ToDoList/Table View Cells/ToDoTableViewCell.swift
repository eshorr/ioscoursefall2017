//
//  ToDoTableViewCell.swift
//  ToDoList
//
//  Created by Eric Shorr on 09/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

protocol ToDoTableViewCellDelegate : NSObjectProtocol
{
    func updateTodo(todo: ToDo?)
}

class ToDoTableViewCell: UITableViewCell {

    @IBOutlet private weak var btnCompleted: UIButton!
    @IBOutlet private weak var lblTitle: UILabel!
    
    weak var delegate:ToDoTableViewCellDelegate?
    
    var todo:ToDo?
    
    func updateTodo(todo: ToDo)
    {
        self.todo = todo
        
        self.btnCompleted.isSelected = (self.todo?.isComplete)!
        
        self.lblTitle.text = self.todo?.title
    }
    
    @IBAction func toggleCompleted(_ sender: Any)
    {
        self.todo?.isComplete = !(self.todo?.isComplete)!
        
        self.btnCompleted.isSelected = (self.todo?.isComplete)!
        
        self.delegate?.updateTodo(todo: self.todo)
    }
    
}
