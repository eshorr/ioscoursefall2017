//
//  ViewController.swift
//  ToDoList
//
//  Created by Eric Shorr on 07/12/2017.
//  Copyright © 2017 Developer Institute. All rights reserved.
//

import UIKit

class ViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,ToDoTableViewCellDelegate,AddToDoDelegate
{
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var todos:[ToDo]?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
//        self.tableView.register(NSClassFromString("UITableViewCell"), forCellReuseIdentifier: "Cell")
        
        
        
        self.tableView.register(UINib(nibName: "ToDoTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        
        if let loadedTodoList : [ToDo] = ToDo.loadToDos(), loadedTodoList.count > 0
        {
            self.todos = loadedTodoList
        }
        else
        {
            self.todos = ToDo.loadSampleToDos()
            ToDo.saveToFile(todos: self.todos!)
        }
        
        self.tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.todos!.count
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : ToDoTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ToDoTableViewCell
        
        let todo:ToDo = self.todos![indexPath.row]
        
        cell.delegate = self
        
        cell.updateTodo(todo: todo)
        
//        cell.textLabel?.text = "\(indexPath.row) title: \(todo.title)"
        
        return cell
    }
    
    func updateTodo(todo: ToDo?)
    {
        print("todo is being updated")
    }
    
    @IBAction func addToDo(_ sender: Any)
    {
        self.performSegue(withIdentifier: "addToDoSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let vc : AddToDoViewController = segue.destination as? AddToDoViewController
        {
            vc.delegate = self
        }
    }
    
    func addToDo(todo: ToDo)
    {
        self.todos?.append(todo)
        
        ToDo.saveToFile(todos: self.todos! )
        
        self.tableView.reloadData()
    }
}

